# Workflow Pull/Merge Request #

* Pull Request : Istilah di Github
* Merge Request : Istilah di Gitlab dan Bitbucket

## Kontributor ##

1. Create branch khusus untuk fitur yang mau dibuat (topic branch)

        git branch fitur-xx
        git checkout fitur-xx

2. Commit di branch tersebut

        git add .
        git commit -m "implement fitur xx"

3. Push ke remote

        git push namaremote namabranch
        git push origin fitur-xx

4. Login ke web, klik Create Merge Request

## Integrator / Reviewer ##

1. Buka merge requestnya di web gitlab

2. Update repo lokal

        git fetch origin

3. Buat branch di local untuk review yang mapping ke remote branch yang mau direview

        git branch review-xx
        git checkout review-xx
        git merge origin/fitur-xx

4. Lakukan review (cek source, test run, dsb)

5. Kalau oke, merge ke master. Bisa via web, bisa juga via local. Pilih salah satu saja.

Cara di local:

        git checkout master
        git merge review-xx
        git push origin master